"""
Tracery Object

This is a joke object that changes its description every time you look at it.
Requires installing pytracery as a pip dependency.

As Kate Compton puts it: "I can easily generate 10,000 bowls of plain oatmeal, with each oat being in a different position and different orientation, and mathematically speaking they will all be completely unique.  But the user will likely just see a lot of oatmeal."

This relies on default Tracery and does not have a model memory (i.e. you cannot procgen object attributs based on its description and vice versa).
"""
from typeclasses.objects import Object
import tracery
from tracery.modifiers import base_english

"""
An oatmeal object has an "origin" attribute.
You make a table of rules with attributes, like:

    set movie/origin:rules = ['This is #genre.a# #movie# about #subject.s#.', '#subject.s.capitalize# #sequel#: The #family.capitalize# Of All #movie.s.capitalize#!']  

The table has to have category "rules" and the main entry point is called "origin". The rest is up to you:

    set movie/sequel:rules = ['from hell #number#','#number#','the Resurrection','Origins','Rebooted','Reimagined','Again']
    set movie/family:rules = ['mother','father','aunt','xyther','grunkle','grandpa','grandma']
    set movie/number:rules = ['2','3','4','5','6','II','III','IV','V','VI','XXIII','MCMXCIII']
    set movie/movie:rules = ['movie', 'flick', 'animation', 'anime', 'moving picture', 'picture']
    set movie/genre:rules = ['action', 'romance', 'thriller', 'horror', 'abstract', 'slice-of-life', 'comedy', 'documentary']
    set movie/subject:rules = ['wombat', '#pair# love #polycule#', 'smartphone', 'astronaut', 'explosion']
    set movie/pair:rules = ['gay','aromantic','nonbinary','clueless','crazy']
    set movie/polycule:rules = ['triange', 'pair', '3D icosahedron', 'polycule']

And you'll get stuff like "Crazy love polycules Reimagined: The Father Of All Animations!"
"""
class OatmealObject(Object):
    """
    Can't be a self.db.desc because we *need* a function here.
    """
    def get_display_desc(self, looker, **kwargs):
        rules = {}
        attrs = self.attributes.get(category='rules', return_obj=True)
        for attr in attrs:
            v = attr.value.deserialize()
            if isinstance(v, list) or isinstance(v, str):
                rules[attr.key] = v
        if rules == {}:
            return "No set of rules detected. Set the intro point with: |wset object/origin:rules = 'text'|n"
        grammar = tracery.Grammar(rules)
        grammar.add_modifiers(base_english)
        return grammar.flatten("#origin#")
    pass
