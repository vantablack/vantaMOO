"""
Characters

Characters are (by default) Objects setup to be puppeted by Accounts.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from lib.rpsystem import ContribRPCharacter

from lib.pronounsub import character as pronounsub

# rpsystem
class Character(ContribRPCharacter):
    def at_object_creation(self):
        pronounsub.object_init(self)

        super().at_object_creation()

    def msg(self, text=None, from_obj=None, session=None, **kwargs):
        text, from_obj, session, kwargs = pronounsub.msg(self, text, from_obj, session, **kwargs)

        super().msg(text, from_obj, session, **kwargs)
