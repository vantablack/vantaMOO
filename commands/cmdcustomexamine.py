from evennia.commands.default.building import CmdExamine

class CmdCustomExamine(CmdExamine):
    def format_attributes(self, obj):
        caller = self.caller
        output = "\n  "
        for attr in obj.db_attributes.all():
            if attr.db_key == "creator_ip" and not caller.permissions.check("Admin"):
                continue
            output += self.format_single_attribute(attr)
            output += "\n  "
        if output.strip():
            # we don't want just an empty line
            return output
