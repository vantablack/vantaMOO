from .common import *
from .character import object_init

class PronounAdminCommand(Command):
    """
    Manages the list of known server-wide pronouns.

    Usage:
      @pronounadmin help
      @pronounadmin list_known
      @pronounadmin spec <specific>
      @pronounadmin unspec <specific>

    On each character and the server are stored character pronouns and specific
    pronouns. The character pronouns specify what pronouns should actually be
    used for your character, and the specific pronouns define all the
    grammatical forms of a pronoun so the character pronouns can be used in
    text substitution.

    The specific format is: <subjective>,<objective>,<possessive>,<absolute>
      subjective: *They* went to the store.
      objective:  You took *them* to the store.
      possessive: You took *their* friend to the store.
      absolute:   The sandwich in the fridge was *theirs*.

    The `list_known` command lists all specific pronouns known by the server.

    The `spec` command adds a new specific pronoun.

    The `unspec` command removes an existing specific pronoun.

    Examples:
      @pronounadmin spec they,them,their,theirs
    """
    key = "pronounadmin"
    locks = "cmd:id(1) or perm(Admin)"

    def parse(self):
        caller = self.caller

        args = self.args.strip().lower().split()
        if len(args) == 0:
            caller.msg(self.get_help(caller, self.cmdset))
            raise InterruptCommand()

        command = args[0]

        if command == "list_known" and len(args) == 1:
            self.args = tuple([command])
            return

        if len(args) != 2:
            caller.msg(self.get_help(caller, self.cmdset))
            raise InterruptCommand()

        if not command in [ "spec", "unspec" ] or command == "help":
            caller.msg(self.get_help(caller, self.cmdset))
            raise InterruptCommand()

        pronoun = parse_specific_pronoun(args[1])
        if pronoun == None:
            caller.msg(self.get_help(caller, self.cmdset))
            raise InterruptCommand()

        self.args = ( command, pronoun )

    def func(self):
        caller = self.caller

        command = self.args[0]

        if command == "list_known":
            caller.msg("Known server-defined pronouns:")
            for spec in get_pronoun_list():
                caller.msg(f"  {stringify_specific_pronoun(spec)}")
            return

        _, pronoun = self.args
        pronoun_list = get_pronoun_list()

        if command == "spec":
            if not pronoun in pronoun_list:
                pronoun_list.append(pronoun)
            self.caller.msg(f"Added the pronoun {stringify_specific_pronoun(pronoun)}.")
        elif command == "unspec":
            if pronoun in pronoun_list:
                pronoun_list.remove(pronoun)
            self.caller.msg(f"Removed the pronoun {stringify_specific_pronoun(pronoun)}.")

class PronounsCommand(Command):
    """
    Manages your pronouns.

    Usage:
      @pronouns help
      @pronouns set <pronouns>
      @pronouns get
      @pronouns list_known
      @pronouns spec <specific>
      @pronouns unspec <specific>

    On each character and the server are stored character pronouns and specific
    pronouns. The character pronouns specify what pronouns should actually be
    used for your character, and the specific pronouns define all the
    grammatical forms of a pronoun so the character pronouns can be used in
    text substitution.

    The server already has some specific pronouns defined, so it's likely you
    can just give your character pronouns and be done. If the server doesn't
    know about your pronouns already then you'll have to tell the server how to
    use them with the `spec` command.

    The specific format is: <subjective>,<objective>,<possessive>,<absolute>
      subjective: *They* went to the store.
      objective:  You took *them* to the store.
      possessive: You took *their* friend to the store.
      absolute:   The sandwich in the fridge was *theirs*.

    The `set` command:

    Sets your current character pronouns. Pronouns look the same as how you'd
    write them anywhere online. They're separated by a slash ("/"), like
    "she/they/it", or "he/him". Each pronoun should match to the grammatical
    form of a specific pronoun. When multiple pronouns are given, like both
    "she" and "they", a random one will be used in each text substitution.

    The `get` command prints your current configured character pronouns.

    The `list_known` command lists all specific pronouns known by the
    server and specified for your acount.

    The `spec` command adds a new specific pronoun.

    The `unspec` command removes an existing specific pronoun.

    Examples:
        @pronouns set he/him

        @pronouns set she/they/it

        @pronouns spec fae,faer,faer,faers
    """

    key = "pronouns"

    def parse(self):
        caller = self.caller
        object_init(caller)

        args = self.args.strip().lower().split()
        if len(args) == 1:
            command = args[0]
            if not command in [ "get", "list_known" ] or command == "help":
                caller.msg(self.get_help(caller, self.cmdset))
                raise InterruptCommand()

            self.args = tuple([command])
            return

        if len(args) != 2:
            caller.msg(self.get_help(caller, self.cmdset))
            raise InterruptCommand()

        command = args[0]

        if command == "spec" or command == "unspec":
            new = parse_specific_pronoun(args[1])
            if new  == None:
                caller.msg(self.get_help(caller, self.cmdset))
                raise InterruptCommand()
            else:
                res = new

        elif command == "set":
            res = args[1].split('/')

        self.args = (command, res)

    def func(self):
        caller = self.caller
        object_init(caller)

        command = self.args[0]

        match self.args[0]:
            case "set":
                _, pronouns = self.args
                has_unmatched = False
                for pronoun in pronouns:
                    if not specific_pronoun_from_character(pronoun, caller):
                        has_unmatched = True
                        caller.msg(f"Warning: The character pronoun \"{pronoun}\" is "
                                   "not in any list of specific pronouns. It will "
                                   "be ignored. |/")
                if has_unmatched:
                    caller.msg("You have unkown pronouns set! If you want to "
                               "actually use them, you'll need to set their "
                               "specific form. Type `pronouns help` for "
                               "details. |/")

                caller.db.pronouns = pronouns
                caller.msg(f"Your pronouns have been set to {stringify_pronouns(caller)}.")

            case "get":
                caller.msg(f"Your pronouns are {stringify_pronouns(caller)}.")

            case  "list_known":
                caller.msg("Known user-defined pronouns:")
                for spec in caller.db.pronoun_specs:
                    caller.msg(f"  {stringify_specific_pronoun(spec)}")
                caller.msg("Known server-defined pronouns:")
                for spec in get_pronoun_list():
                    caller.msg(f"  {stringify_specific_pronoun(spec)}")

            case "spec":
                _, pronoun = self.args
                if not pronoun in caller.db.pronoun_specs:
                    caller.db.pronoun_specs.append(pronoun)
                self.caller.msg(f"Added the pronoun {stringify_specific_pronoun(pronoun)}.")

            case "unspec":
                _, pronoun = self.args
                if pronoun in caller.db.pronoun_specs:
                    caller.db.pronoun_specs.remove(pronoun)
                self.caller.msg(f"Remobed the pronoun {stringify_specific_pronoun(pronoun)}.")
