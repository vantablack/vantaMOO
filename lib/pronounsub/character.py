from .common import *

# This should be called before every use of a Character to make sure it's
# initialized right.
def object_init(character):
    if character.db.pronouns == None:
        character.db.pronouns = DEFAULT_CHARACTER_PRONOUNS[:]
    if character.db.pronoun_specs == None:
        character.db.pronoun_specs = []

def msg(character, text=None, from_obj=None, session=None, **kwargs):
    object_init(character)

    """
    Emits something to a session attached to the object.
    Overloads the default msg() implementation to include
    gender-aware markers in output.

    Args:
        text (str or tuple, optional): The message to send. This
            is treated internally like any send-command, so its
            value can be a tuple if sending multiple arguments to
            the `text` oob command.
        from_obj (obj, optional): object that is sending. If
            given, at_msg_send will be called
        session (Session or list, optional): session or list of
            sessions to relay to, if any. If set, will
            force send regardless of MULTISESSION_MODE.
        Note:
        `at_msg_receive` will be called on this Object.
        All extra kwargs will be passed on to the protocol.

    """
    if text is None:
        super().msg(from_obj=from_obj, session=session, **kwargs)
        return

    try:
        repl = lambda a: pronoun_repl(character, a)

        if text and isinstance(text, tuple):
            text = (RE_GENDER_PRONOUN.sub(repl, text[0]), *text[1:])
        else:
            text = RE_GENDER_PRONOUN.sub(repl, text)
    except TypeError:
            pass
    except Exception as e:
        logger.log_trace(e)
    return (text, from_obj, session, kwargs)
